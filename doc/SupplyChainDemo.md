# SupplyChainDemo


* [Quotation](#contract-quotation)

* [ShippingMethod](#contract-shippingmethod)

* [Contract](#contract-contract)


## contract: Quotation

    overview:
	function AddNewQuotation() public  






#### Quotation properties

name|type|visiblity|delegate|doc
----|----|----|----|----
suppliermanagement|SupplierManagement|public||
TransactionID||public||
IssueDate||public||
FreightTerms||public||
PaymentTerms||public||
-

#### Quotation.AddNewQuotation() public  



#### event GetPrice




## contract: ShippingMethod

    overview:






#### ShippingMethod properties

name|type|visiblity|delegate|doc
----|----|----|----|----
warehouse|Warehouse|public||
-


## contract: Contract

    overview:






#### Contract properties

name|type|visiblity|delegate|doc
----|----|----|----|----
purchaseorder|PurchaseOrder|public||
-


